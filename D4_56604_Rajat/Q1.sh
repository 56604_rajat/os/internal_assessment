#!/usr/bin/bash

clear

echo "Enter a limit"
read limit
echo "prime numbers upto $limit are :"
    i=2
    while [ $i -le $limit ]  #(i<limit)
    do
        flag=1
        j=2    
        while [ $j -lt $i ]  #(j< i)
        do
         
            if [ `expr $i % $j ` -eq 0 ]  #(i%j==0)
            then
                flag=0
                break
            fi
            j=$(( $j+1 ))  #j++
        done
        if [ $flag -eq 1 ]    
        then
            echo "$i"
        fi
        i=$(( $i+1 ))   #i++
    done
exit