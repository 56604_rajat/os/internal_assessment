#!/usr/bin/bash
clear

echo -n "enter the number to check whether is is perfect square or not : "
read n


    i=1
    status=0
    while [ `expr $i \* $i` -le $n ] 
    
    do
        
        if [ `expr $n % $i` -eq 0 -a `expr $n / $i` -eq $i ]
        then
        status=1
                    
        fi

        i=`expr $i + 1`
    done

    if [ $status -eq 1 ] 
    then
        echo "$n is perfect square" 
    else
        echo "$n is not perfect square" 
    fi

exit