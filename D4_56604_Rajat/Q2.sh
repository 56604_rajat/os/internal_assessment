#!/usr/bin/bash
clear
echo "Enter degree celsius temperature: " 
read celsius
    fahrenheit=`echo "$celsius*1.8 + 32" | bc`
    echo "$celsius degree celsius is equal to $fahrenheit degree fahrenheit"
exit